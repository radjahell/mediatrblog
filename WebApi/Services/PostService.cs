﻿using System;
using System.Collections.Generic;
using MediatrBlog.WebApi.Contracts;
using MediatrBlog.WebApi.Contracts.Interfaces;
using MediatrBlog.WebApi.Repositories;

namespace MediatrBlog.WebApi.Services
{  
    public class PostService : IPostService
    {
        private readonly IPostRepository _postRepository;

        public PostService(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }

        public IEnumerable<Post> Get()
        {
            return _postRepository.Get();
        }

        public Post Get(long id)
        {
            return _postRepository.Get(id);
        }

        public void Create(Post post)
        {
            _postRepository.Create(post);
        }

        public void Delete(long id)
        {
            _postRepository.Delete(id);
        }

        public void Update(long id, Post post)
        {
            _postRepository.Update(id, post);
        }

        public void AddTagToPost(long id, Tag tag)
        {
            _postRepository.AddTagToPost(id, tag);
        }
    }
}
