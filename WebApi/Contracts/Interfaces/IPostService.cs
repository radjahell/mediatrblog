﻿using System;
using System.Collections.Generic;

namespace MediatrBlog.WebApi.Contracts.Interfaces
{
    public interface IPostService
    {
        IEnumerable<Post> Get();
        Post Get(long id);
        void Create(Post post);
        void Delete(long id);
        void Update(long id, Post post);
        void AddTagToPost(long id, Tag tag);
    }
}
