﻿using System.Collections.Generic;
using MediatrBlog.WebApi.Contracts;

namespace MediatrBlog.WebApi.Contracts.Interfaces
{
    public interface IPostRepository
    {
        void AddTagToPost(long id, Tag tag);
        void Create(Post post);
        void Delete(long id);
        IEnumerable<Post> Get();
        Post Get(long id);
        void Update(long id, Post post);
    }
}