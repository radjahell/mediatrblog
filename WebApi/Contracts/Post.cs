﻿using System;
using System.Collections.Generic;

namespace MediatrBlog.WebApi.Contracts
{
    public class Post
    {
        public long Id { get; set; }
        public string Header { get; set; }
        public string Body { get; set; }
        public DateTime TimeStamp { get; set; }
        public List<Tag> Tags { get; set; } 
    }
}
