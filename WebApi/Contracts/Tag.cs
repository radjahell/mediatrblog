﻿using System;
using System.Collections.Generic;

namespace MediatrBlog.WebApi.Contracts
{
    public class Tag
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime TimeStamp { get; set; }
        public List<Post> Posts { get; set; }
    }
}
