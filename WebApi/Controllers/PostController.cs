﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc; 
using MediatrBlog.WebApi.Services;
using MediatrBlog.WebApi.Contracts;
using MediatrBlog.WebApi.Contracts.Interfaces;

namespace MediatrBlog.WebApi.Controllers
{
    [Route("posts")] 
    public class PostController : Controller
    {
        private readonly IPostService _postService;

        public PostController(IPostService postService)
        {
            _postService = postService;
        } 

        [HttpGet]
        public ActionResult<IEnumerable<Post>> Get()
        {
            return Ok(_postService.Get());
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<Post>> Get(long id)
        {
            return Ok(_postService.Get(id));
        }

        [HttpPost]
        public void Post([FromBody] Post post)
        { 
            _postService.Create(post); 
        }

        [HttpPut("{id}/tags")]
        public void Post(long id,[FromBody] Tag tag)
        {
            _postService.AddTagToPost(id, tag);
        }  

        [HttpGet("tags")]
        public ActionResult<IEnumerable<Tag>> GetTags()
        {
            return Ok(_postService.Get());
        } 
    }
}
