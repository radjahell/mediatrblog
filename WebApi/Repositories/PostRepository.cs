﻿using System;
using System.Collections.Generic;
using LiteDB;
using MediatrBlog.WebApi.Contracts;
using MediatrBlog.WebApi.Contracts.Interfaces;

namespace MediatrBlog.WebApi.Repositories
{ 
    public class PostRepository : IPostRepository
    {
        private const string LiteDbName = "Filename=MyData.db;Mode=Exclusive";


        public IEnumerable<Post> Get()
        {
            using (var db = new LiteDatabase(LiteDbName))
            {
                var collection = db.GetCollection<Post>("posts");
                return collection.FindAll();
            }
        }

        public Post Get(long id)
        {
            using (var db = new LiteDatabase(LiteDbName))
            {
                var collection = db.GetCollection<Post>("posts");
                return collection.FindById(id);
            }
        }

        public void Create(Post post)
        {
            using (var db = new LiteDatabase(LiteDbName))
            {
                var collection = db.GetCollection<Post>("posts");
                collection.EnsureIndex(x => x.Id);
                collection.Insert(post);
            }
        }

        public void Delete(long id)
        {
            using (var db = new LiteDatabase(LiteDbName))
            {
                var collection = db.GetCollection<Post>("posts");
                collection.Delete(id);
            }
        }

        public void Update(long id, Post post)
        {
            using (var db = new LiteDatabase(LiteDbName))
            {
                var collection = db.GetCollection<Post>("posts");
                var postx = collection.FindById(id);
                //Not correct
                collection.Update(post); 
            }
        }

        public void AddTagToPost(long id, Tag tag)
        {
            using (var db = new LiteDatabase(LiteDbName))
            {
                var collection = db.GetCollection<Post>("posts");
                var postx = collection.FindById(id);
                postx.Tags.Add(tag);
                //Not correct
                collection.Update(postx);


            }
        }
    }
}
